"""
Розробити функцію convert_n_to_m(x, n, m), 
яка приймає 3 аргументи -- ціле число (в системі числення з основою n) або рядок x, що представляє таке число, та цілі числа n та m (1 <= n, m <= 36), 
та повертає рядок -- представлення числа х у системі числення m.
У випадку, якщо аргумент х не є числом або рядком, або не може бути представленням цілого невід'ємного числа в системі числення з основою n, повернути логічну константу False.
В системах числення з основою більше десяти для позначення розрядів із значенням більше 9 використовувати літери латинського алфавіту у верхньому регістрі від A до Z. У вхідному x можуть використовуватися обидва регістри.
Вважати, що в одиничній системі числення число записується відповідною кількістю нулів.
Наприклад
Виклик функції: convert_n_to_m([123], 4, 3)
Повертає: False
Виклик функції: convert_n_to_m("0123", 5, 6)
Повертає: 102
Виклик функції: convert_n_to_m("123", 3, 5)
Повертає: False
Виклик функції: convert_n_to_m(123, 4, 1)
Повертає: 000000000000000000000000000
Виклик функції: convert_n_to_m(-123.0, 11, 16)
Повертає: False
Виклик функції: convert_n_to_m("A1Z", 36, 16)
Повертає: 32E7
"""


def convert_n_to_m(x, n, m):
    tenth = 0
    new = [[]]
    numbers = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for i in range(len(str(x))):
        if n == 1:
            tenth += 1
        else:
            tenth += numbers.find(str(x)[i]) * (n ** (len(str(x)) - (i + 1)))
    if m == 1:
        return "0" * tenth
    elif max(str(x)) == '0':
        return 0
    elif (type(x) is str or type(x) is int) and n > numbers.find(max(str(x))) and m <= 36:
        while (m ** (len(new))) // tenth == 0:
            new.append([])
        for i in range(len(new)):
            new[i] = numbers[tenth // (m ** (len(new) - (i + 1)))]
            tenth -= (m ** (len(new) - (i + 1))) * int(numbers.find(new[i]))
        return ''.join(new)
    else:
        return False

print(convert_n_to_m("7A84EE", 16, 26))  # HELLO
print(convert_n_to_m([123], 4, 3))  # False
print(convert_n_to_m("1110100110", 2, 36))  # PY
print(convert_n_to_m("0123", 5, 6))  # 102
print(convert_n_to_m("123", 3, 5))  # False
print(convert_n_to_m("3G1LN", 36, 10))  # 5787419
print(convert_n_to_m(123, 4, 1))  # 000000000000000000000000000
print(convert_n_to_m(-123.0, 11, 16))  # False
print(convert_n_to_m("A1Z", 36, 16))  # 32E7